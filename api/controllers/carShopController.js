'use strict';
const path = require('path')
const fs = require('fs');

exports.list_all_facts = function(req, res) {
    var carsInShop = readFile();
    res.json(carsInShop.table);
};

exports.create_car_in_shop = function(req, res) {
    var carsInShop = readFile();
    carsInShop.table.push(req.body);
    writeToFile(carsInShop);
    res.json(200);
};

exports.delete_car_in_shop = function(req, res) {
    var carsInShop = readFile();
    carsInShop.table.forEach(function (element, index){
        if (element.id == req.params.carId) {
            carsInShop.table.splice(index, 1);
        }
    });
    writeToFile(carsInShop);
    res.json(carsInShop.table)
    
};

function writeToFile(data)
{
    return fs.writeFileSync((path.dirname(require.main.filename) + '/api/resources/registeredCarsInShop.json'),
        JSON.stringify(data));
}

function readFile() {
    let rawData = fs.readFileSync(path.dirname(require.main.filename) + '/api/resources/registeredCarsInShop.json');
    let carsInShop = JSON.parse(rawData);
    return carsInShop;
}