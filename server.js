var express = require('express'),
    app = express(),
    port = 8080,
    bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});


var routes = require('./api/routes/carShopRoutes'); //importing route
routes(app); //register the route


app.listen(port);


console.log('Car shop RESTful API server started on: ' + port);
