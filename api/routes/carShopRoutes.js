'use strict';
module.exports = function(app) {
    var facts = require('../controllers/carShopController');

    // Car Routes
    app.route('/api/getAllCars')
        .get(facts.list_all_facts);

    app.route('/api/deleteCar/:carId')
        .get(facts.delete_car_in_shop);
    app.route('/api/addCar')
        .post(facts.create_car_in_shop);
};
